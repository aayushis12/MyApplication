package com.example.aayushi.myapplication;

import android.accessibilityservice.AccessibilityService;
import android.graphics.PixelFormat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.Button;
import android.widget.FrameLayout;

import java.util.List;

public class MyAccessibilityService extends AccessibilityService {
    FrameLayout mLayout; //9742612318

    @Override
    protected void onServiceConnected() {
        WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);
        mLayout = new FrameLayout(this);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.type = WindowManager.LayoutParams.TYPE_ACCESSIBILITY_OVERLAY;
        lp.format = PixelFormat.TRANSLUCENT;
        lp.flags |= WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.TOP;
        LayoutInflater inflater = LayoutInflater.from(this);
        inflater.inflate(R.layout.activity_main, mLayout);
        wm.addView(mLayout, lp);
        configurePowerButton();
    }

    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {
        Log.i("ldkls",event.toString());
        AccessibilityNodeInfo source = event.getSource();
        if(source==null)return;
        Log.i("source--------", source.toString());
        List<AccessibilityNodeInfo> nodeInfo = source.findAccessibilityNodeInfosByViewId("com.example.aayushi.myapplication:id/power");
        if (nodeInfo.size()>0) {
            AccessibilityNodeInfo parent = nodeInfo.get(0);
            // You can also traverse the list if required data is deep in view hierarchy.
            String requiredText = parent.getText().toString();
            Log.i("Required Text", requiredText);
        }
    }

    @Override
    public void onInterrupt() {

    }

    private void configurePowerButton() {
        Button powerButton = (Button) mLayout.findViewById(R.id.power);
        powerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                performGlobalAction(GLOBAL_ACTION_POWER_DIALOG);
            }
        });
    }
}
